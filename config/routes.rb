Rails.application.routes.draw do
  root 'home#index'

  # root 'events#index'
  # resources :events


  get '/events/:id', to: 'home#index'
  get '/new_event', to: 'home#index'
  get '/confirm', to: 'home#index'

  namespace :api do
    resources :events
  end

  namespace :admin do
    resources :events
  end

  # match '*path', to: 'home#index', via: :all
end
