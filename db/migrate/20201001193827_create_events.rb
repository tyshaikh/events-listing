class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :title
      t.string :location
      t.string :link
      t.datetime :date
      t.text :summary
      t.text :description

      t.timestamps
    end
  end
end
