# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Event.create(
  title: "Data Science Day 2020",
  location: "Virtual",
  date: DateTime.new(2020, 9, 14),
  link: "https://heldata.fi/",
  summary: "As part of the Data Science Institute's flagship annual event, this virtual session will bring together thought leaders who are driving discussions on ethics and privacy in data science and engineering.",
  description: "Helsinki Data Science Day is a great opportunity for the community to learn and share knowledge, best practices, and new discoveries. The idea is to bring the experts together to discuss topics – even technical ones – on state-of-art data science and statistics, and to share ideas and experiences.<br><br>The event is organized in a fun and friendly way, and both the attendees and organizers are drawn from the community. Helsinki Data Science Day 2020 is a chance for the community and your company to connect, get acquinted with the latest technology, and glimpse the latest research in the field. Our attendees consist of employees from the hottest companies, researchers from the best universities, and the top experts in the field.<br><br>Helsinki Data Science Day event started in 2016 as a project within the organisation of statistics students in the University of Helsinki – Moodi ry – and has since been organized each year except 2017, with the 2019 event being first independently organized Helsinki Data Science Day. In addition, Helsinki Data Science Day has retained strong connections with the University of Helsinki and Aalto University. The event has been growing each year, and the year 2020 will be no different! You can check out the past events at the Past Events section."
)
