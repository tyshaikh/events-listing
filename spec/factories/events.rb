# spec/factories/events.rb
FactoryBot.define do
  factory :event do
    title { Faker::App.name }
    location { Faker::Address.city }
    date { Faker::Date.forward(days: 23) }
    link { Faker::Internet.domain_name }
    summary { Faker::Lorem.paragraph }
    description { Faker::Lorem.paragraphs }
  end
end
