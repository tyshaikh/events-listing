# spec/requests/events_spec.rb
require 'rails_helper'

RSpec.describe 'Events API', type: :request do
  # initialize test data
  let!(:events) { create_list(:event, 10) }
  let(:event_id) { events.first.id }

  # Test suite for GET /api/events
  describe 'GET /api/events' do
    # make HTTP get request before each example
    before { get '/api/events' }

    it 'returns events' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /api/events/:id
  describe 'GET /api/events/:id' do
    before { get "/api/events/#{event_id}" }

    context 'when the record exists' do
      it 'returns the todo' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(event_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:event_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Event/)
      end
    end
  end

  # Test suite for POST /api/events
  describe 'POST /api/events' do
    # valid payload
    let(:valid_attributes) { {
      title: "Data Science Day 2020",
      location: "Virtual",
      date: DateTime.new(2020, 9, 14),
      link: "https://heldata.fi/",
      summary: "As part of the Data Science Institute's flagship annual event, this virtual session will bring together thought leaders who are driving discussions on ethics and privacy in data science and engineering.",
      description: "Helsinki Data Science Day is a great opportunity for the community to learn and share knowledge, best practices, and new discoveries. The idea is to bring the experts together to discuss topics – even technical ones – on state-of-art data science and statistics, and to share ideas and experiences.<br><br>The event is organized in a fun and friendly way, and both the attendees and organizers are drawn from the community. Helsinki Data Science Day 2020 is a chance for the community and your company to connect, get acquinted with the latest technology, and glimpse the latest research in the field. Our attendees consist of employees from the hottest companies, researchers from the best universities, and the top experts in the field.<br><br>Helsinki Data Science Day event started in 2016 as a project within the organisation of statistics students in the University of Helsinki – Moodi ry – and has since been organized each year except 2017, with the 2019 event being first independently organized Helsinki Data Science Day. In addition, Helsinki Data Science Day has retained strong connections with the University of Helsinki and Aalto University. The event has been growing each year, and the year 2020 will be no different! You can check out the past events at the Past Events section."
    } }

    context 'when the request is valid' do
      before { post '/api/events', params: valid_attributes }

      it 'creates an event' do
        expect(json['title']).to eq('Data Science Day 2020')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/events', params: { title: 'Data Science Day 2020' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed/)
      end
    end
  end

  # Test suite for PUT /api/events/:id
  describe 'PUT /api/events/:id' do
    let(:valid_attributes) { { title: 'Data Science Day 2021' } }

    context 'when the record exists' do
      before { put "/api/events/#{event_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /api/events/:id
  describe 'DELETE /api/events/:id' do
    before { delete "/api/events/#{event_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

end
