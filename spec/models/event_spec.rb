# spec/models/event_spec.rb
require 'rails_helper'

# Test suite for the Event model
RSpec.describe Event, type: :model do
  # Association test

  # Validation tests
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:location) }
  it { should validate_presence_of(:date) }
  it { should validate_presence_of(:link) }
  it { should validate_presence_of(:summary) }
  it { should validate_presence_of(:description) }

end
