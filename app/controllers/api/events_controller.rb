class API::EventsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  def index
    @events = Event.all
    json_response(@events)
  end

  def show
    json_response(@event)
  end

  def new
    @event = Event.new
    json_response(@event)
  end

  def edit
    json_response(@event)
  end

  def create
    @event = Event.create!(event_params)
    json_response(@event, :created)
  end

  def update
    @event.update(event_params)
    head :no_content
    # json_response(@event)
  end

  def destroy
    @event.destroy
    head :no_content
  end

  private

  def event_params
    params.permit(:title, :location, :date, :link, :summary, :description, :photo)
  end

  def set_event
    @event = Event.find(params[:id])
  end

end
