class Admin::EventsController < ApplicationController
  layout 'admin'
  http_basic_authenticate_with name: "tyshaikh", password: "staples1951"
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  def index
    @events = Event.all.sort_by &:date
  end

  def show
  end

  def new
    @event = Event.new
  end

  def edit
  end

  def create
    @event = Event.new(event_params)

    if @event.save
      redirect_to admin_events_path
    else
      render 'new'
    end
  end

  def update
    if @event.update(event_params)
      redirect_to admin_events_path
    else
      render 'edit'
    end
  end

  def destroy
    @event.destroy

    redirect_to admin_events_path
  end

  private

  def event_params
    params.require(:event).permit(:title, :location, :date, :link, :summary, :description, :photo)
  end

  def set_event
    @event = Event.find(params[:id])
  end

end
