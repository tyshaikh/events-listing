import React from "react"
import PropTypes from "prop-types"
import { Link } from 'react-router-dom'

class Confirm extends React.Component {
  render () {
    return (
      <React.Fragment>
        <div className="flex items-center rounded shadow bg-white my-8 p-5">
          <div className="p-8">
            <p className="text-xl lg:text-lg">
              Thanks for submitting a new event!
              <Link to="/"><span className="text-blue-600"> Go back to the calendar</span></Link>
            </p>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Confirm
