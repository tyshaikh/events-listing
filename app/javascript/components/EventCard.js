import React from "react"
import PropTypes from "prop-types"

class EventCard extends React.Component {

  parseDate = (val) => {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    let parsed = Date.parse(val)
    let newDate = new Date(parsed)
    let strDate = newDate.toLocaleDateString("en-US", options)
    return strDate
   }

   generateImgUrl = () => {
     let host = "http://localhost:3000"
     let imgUrl = ""

     if (this.props.event.attachment_url == null) {
       imgUrl = "/assets/conf-generic-efd693bfb2bc230316529e676f6cc1c9afa56913b111de0b542238d59a8fe719.jpg"
     } else {
       imgUrl = this.props.event.attachment_url
     }

     let combinedUrl = host + imgUrl
     return combinedUrl
   }

  render () {
    return (
      <React.Fragment>
        <section className="mt-8">
          <div className="flex items-center rounded shadow bg-white mb-8 p-5">
            <img src={this.generateImgUrl()} className="w-1/3 rounded mr-10" />
            <div className="px-8">
              <div className="mb-4">
                <p className="text-blue-600 font-bold leading-wide uppercase text-gray-600 text-xl lg:text-base mb-1">
                  {this.parseDate(this.props.event.date)}
                </p>
                <p className="font-semibold text-3xl lg:text-2xl mb-1">
                  {this.props.event.title}
                </p>
                <p className="text-gray-600 text-xl lg:text-base">
                  {this.props.event.location}
                </p>
              </div>
              <p className="text-xl lg:text-base">
                {this.props.event.summary}
              </p>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default EventCard
