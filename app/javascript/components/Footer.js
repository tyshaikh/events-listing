import React from "react"
import PropTypes from "prop-types"
class Footer extends React.Component {
  render () {
    return (
      <React.Fragment>
        <section className="border-solid border-t mt-40 mb-8 pt-8">
          <div className="text-center text-gray-600">
            <p>
              <a href="https://www.linkedin.com/in/shaikht/" target="_blank" className="text-blue-500">Made by Ty</a>
            </p>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default Footer
