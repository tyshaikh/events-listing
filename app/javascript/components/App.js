import React from "react"
import PropTypes from "prop-types"
import { Route, Switch } from 'react-router-dom'

import EventsListing from './EventsListing'
import EventDetails from './EventDetails'
import NewEvent from './NewEvent'
import Confirm from './Confirm'
import Footer from './Footer'

import './App.css';

class App extends React.Component {
  render () {
    return (
      <React.Fragment>
        <div className="container mx-auto w-full lg:w-3/4">
          <Switch>
            <Route exact path="/" component={EventsListing} />
            <Route exact path="/events/:id" component={EventDetails} />
            <Route exact path="/new_event" component={NewEvent} />
            <Route exact path="/confirm" component={Confirm} />
          </Switch>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

export default App
