import React from "react"
import PropTypes from "prop-types"
import { Link } from 'react-router-dom';

import Header from './Header'
import EventCard from './EventCard'

class EventsListing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: []
    };
  }

  componentDidMount(){
    fetch('/api/events')
      .then((response) => {return response.json()})
      .then((data) => {this.setState({ events: data }) });
  }

  renderAllEvents = () => {
     return(
       <div>
         {this.state.events.map(event => (
           <div key={event.id}>
            <Link to={`/events/${event.id}`}>
              <EventCard event={event} />
            </Link>
           </div>
         ))}
       </div>
     )
   }

  render () {
    return (
      <React.Fragment>
        <Header />
        {this.renderAllEvents()}
      </React.Fragment>
    );
  }
}

export default EventsListing
