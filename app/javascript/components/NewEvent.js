import React from "react"
import PropTypes from "prop-types"
import { Link, Redirect } from 'react-router-dom'

import Confirm from './Confirm'

class NewEvent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      date: '',
      location: '',
      link: '',
      photo: '',
      summary: '',
      description: '',
      toHome: false
    };

    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleLinkChange = this.handleLinkChange.bind(this);
    this.handlePhotoChange = this.handlePhotoChange.bind(this);
    this.handleSummaryChange = this.handleSummaryChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);

    this.resetState = this.resetState.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleTitleChange = (e) => {
    this.setState({title: e.target.value});
  }

  handleLocationChange = (e) => {
    this.setState({location: e.target.value});
  }

  handleDateChange = (e) => {
    this.setState({date: e.target.value});
  }

  handleLinkChange = (e) => {
    this.setState({link: e.target.value});
  }

  handlePhotoChange = (e) => {
    this.setState({photo: e.target.files[0]});
  }

  handleSummaryChange = (e) => {
    this.setState({summary: e.target.value});
  }

  handleDescriptionChange = (e) => {
    this.setState({description: e.target.value});
  }

  resetState = () => {
    this.setState({
      title: '',
      date: '',
      location: '',
      link: '',
      photo: '',
      summary: '',
      description: '',
      toHome: true
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    data.set('title', this.state.title);
    data.set('location', this.state.location);
    data.set('date', this.state.date);
    data.set('link', this.state.link);
    data.set('summary', this.state.summary);
    data.set('description', this.state.description);
    data.set('photo', this.state.photo);

    fetch('/api/events', {
      method: 'POST',
      body: data,
    });

    this.resetState();
  }

  render () {
    if (this.state.toHome === true) {
      return <Redirect to='/confirm' />
    }

    return (
      <React.Fragment>
        <div className="rounded shadow my-8 bg-white w-full lg:w-2/3 p-10 mx-auto">
          <div className="font-bold text-2xl text-center">
            Add a new event
          </div>
          <form onSubmit={this.handleSubmit} className="w-full">
            <div className="mb-4">
              <label className="block text-gray-700 lg:text-sm font-bold mb-2">
                Event Title
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                placeholder="ODSC Conference 2020"
                type="text"
                value={this.state.title}
                onChange={this.handleTitleChange}
                required />
              <p className="text-gray-600 text-sm lg:text-xs italic">Please specify a title for the event.</p>
            </div>

            <div className="mb-4">
              <label className="block text-gray-700 lg:text-sm font-bold mb-2">
                Event Date
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                type="date"
                value={this.state.date}
                onChange={this.handleDateChange}
                required />
              <p className="text-gray-600 text-sm lg:text-xs italic">Please specify the date of the event.</p>
            </div>

            <div className="mb-4">
              <label className="block text-gray-700 lg:text-sm font-bold mb-2">
                Event Location
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                value={this.state.location}
                onChange={this.handleLocationChange}
                required />
                <p className="text-gray-600 text-sm lg:text-xs italic">Please specify the city, state or country. If virtual, simply put "Virtual" or "Remote".</p>
            </div>

            <div className="mb-4">
              <label className="block text-gray-700 lg:text-sm font-bold mb-2">
                Event Graphic <span className="text-gray-600, text-xs font-light">(optional)</span>
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                type="file"
                accept="image/jpg, image/jpeg, image/png, image/gif, image/tiff, image/tif, image/svg"
                onChange={this.handlePhotoChange}
                required />
              <p className="text-gray-600 text-xs italic">Please make sure the file is less than 1 MB and is either a JPG or PNG file.</p>
            </div>

            <div className="mb-4">
              <label className="block text-gray-700 lg:text-sm font-bold mb-2">
                Event Website
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                type="url"
                value={this.state.link}
                onChange={this.handleLinkChange}
                required />
              <p className="text-gray-600 text-sm lg:text-xs italic">You can link to a ticketing page or another website.</p>
            </div>

            <div className="mb-4">
              <label className="block text-gray-700 lg:text-sm font-bold mb-2">
                Event Summary
              </label>
              <textarea
                name="summary"
                rows="4"
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                value={this.state.summary}
                onChange={this.handleSummaryChange} />
              <p className="text-gray-600 text-sm lg:text-xs italic mt-2">Provide a short summary of the event.</p>
            </div>

            <div className="mb-4">
              <label className="block text-gray-700 lg:text-sm font-bold mb-2">
                Event Description
              </label>
              <textarea
                name="description"
                rows="10"
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                value={this.state.description}
                onChange={this.handleDescriptionChange} />
              <p className="text-gray-600 text-sm lg:text-xs italic mt-2">Provide a long description of the event.</p>
            </div>
            <div className="flex items-center justify-between">
              <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                Submit event
              </button>
              <Link to="/">
                <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                  Go back
                </button>
              </Link>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default NewEvent
