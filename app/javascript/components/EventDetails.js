import React from "react"
import PropTypes from "prop-types"
import { Link } from 'react-router-dom';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

import EventCard from './EventCard'

class EventDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      event: []
    };
  }

  componentDidMount(){
    fetch(`/api/events/${this.props.match.params.id}`)
      .then((response) => {return response.json()})
      .then((data) => {this.setState({ event: data }) });
  }

  render () {
    return (
      <React.Fragment>
        <section className="my-8 w-full mx-auto">
          <div>
            <Link to="/">
              <p className="text-red-500 font-semibold">
                &larr; Back to all events
              </p>
            </Link>
          </div>
        </section>
        <EventCard event={this.state.event} />
        <section className="mt-16">
          <div className="my-8">
            <p className="text-xl uppercase text-gray-700 font-semibold tracking-widest">
              Event Details
            </p>
          </div>
          <div className="flex items-center rounded shadow bg-white mb-8 p-5">
            <div className="p-8">
              <p className="text-xl lg:text-lg">
                {ReactHtmlParser(this.state.event.description)}
              </p>
            </div>
          </div>
          <div className="">
            <a href={this.state.event.link} target="_blank">
              <button className="bg-blue-500 hover:bg-blue-700 text-2xl lg:text-base text-white font-bold py-2 px-4 rounded">
                Learn more
              </button>
            </a>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default EventDetails
