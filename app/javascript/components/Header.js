import React from "react"
import PropTypes from "prop-types"
import { Link } from 'react-router-dom';

class Header extends React.Component {
  render () {
    return (
      <React.Fragment>
        <section className="mt-10">
          <div className="text-center">
            <div className="text-6xl font-bold mb-2">
              Data Science Conferences
            </div>
            <div className="text-3xl lg:text-2xl text-gray-600 mb-6 max-w-3xl m-auto">
              Find out about upcoming data science conferences around the world.
            </div>
            <Link to="/new_event">
              <button className="bg-blue-500 hover:bg-blue-700 text-2xl lg:text-base text-white font-bold py-2 px-4 rounded">
                Submit an event
              </button>
            </Link>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default Header
