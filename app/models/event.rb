class Event < ApplicationRecord
  include ActiveModel::Serializers::JSON

  after_validation :set_slug, only: [:create, :update]
  has_one_attached :photo

  validates_presence_of :title, :location, :date, :link, :summary, :description

  def to_param
    "#{id}-#{slug}"
  end

  def attributes
    {
      'id' => nil,
      'title' => nil,
      'location' => nil,
      'date' => nil,
      'link' => nil,
      'summary' => nil,
      'description' => nil,
      'updated_at' => nil,
      'created_at' => nil,
      'attachment_url' => nil
    }
  end

  def attachment_url
    if photo.attached?
      Rails.application.routes.url_helpers.rails_representation_url(
        photo.variant(resize_to_limit: [350, 240]).processed, only_path: true
      )
    end
  end

  private

  def set_slug
    self.slug = title.to_s.parameterize
  end

end
