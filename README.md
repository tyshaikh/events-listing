# Event Listing App

A modern events calendar for tech organizations.

## Local Setup

1. Install the gems: `bundle install`
2. Setup the database: `rake db:setup`
3. Running tests: `rspec`
4. Run server: `rails server`
